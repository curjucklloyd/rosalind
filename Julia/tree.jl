#!/usr/bin/env julia


function main()
    n_nodes = 0
    pairs = []
    open(ARGS[1], "r") do f
        n_nodes = f |> readline |> x->parse(Int, x)
        pairs = readlines(f) .|> split .|> pair->(parse(Int, pair[1]),parse(Int,pair[2]))
    end
    
    ss = [] # global list of all isolated subgraphs with at least two nodes
    while !isempty(pairs)
        s = Set()
        toremove = [0]
        while !isempty(toremove)
            toremove = []
            for (i,(n1,n2)) in enumerate(pairs) 
                if ((n1 in s) | (n2 in s)) | isempty(s)
                    push!(s, n1,n2)
                    push!(toremove, i)
                end
            end
            deleteat!(pairs, toremove)
        end
        push!(ss, s)
    end
    n_subgr = length(ss) + length(setdiff(Set(1:n_nodes), union(ss...))) # now include all isolated nodes
    needed_edges = n_subgr - 1
    println(needed_edges)
end


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end

#=
Damn, the proper solution turned out to be dead simple:
as the given graph has no cycles (is a tree actually),
when it will be completed it will have n-1 edges, where
n is number of nodes (which is given at the first line
of the testing dataset). So now all is needed here is to
subtract m given nodes from n-1. The answer is n-m-1 and
this script makes quite a few redundand calcullations.
None the less, it is by design, works not only with trees,
but with any undirected graph, computing its connectivity.
=#
