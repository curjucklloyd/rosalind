#!/usr/bin/env julia


const MODULO = 1_000_000

function main()
    n = 0
    open(ARGS[1], "r") do f
        n = f |> readline |> x->parse(Int, x)
    end
    big(2)^n % MODULO |> println
end


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end
