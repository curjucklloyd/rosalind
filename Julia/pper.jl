#! /usr/bin/env julia


const MODULO = 1_000_000

pper(n, k) = k == 1 ? n : (n-k+1) * pper(n, k-1) % MODULO

function main()
    n = k = 0
    open(ARGS[1], "r") do f
        n, k = f |> readline |> split .|> x->parse(Int, x)
    end
    pper(n, k) |> println


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end
