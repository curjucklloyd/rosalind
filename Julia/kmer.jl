#!/usr/bin/env julia
import Base.Iterators.product


function main()
    seq = ""
    open(ARGS[1], "r") do f
        readline(f) #skip the header line
        seq = join(readlines(f), "")
    end
    
    d = Dict()
    for i=1:length(seq)-3
        mer = seq[i:i+3]
        haskey(d, mer) ? d[mer] += 1 : d[mer] = 1
    end

    for comb in product("ACGT", "ACGT", "ACGT", "ACGT")
        mer = comb |> reverse |> join # appearing in abc order, but need reversing
        haskey(d, mer) ? print(d[mer], " ") : print("0 ")
    end
    println()
end


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end
