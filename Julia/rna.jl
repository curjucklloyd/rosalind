#!/usr/bin/env julia


function transcribe(seq)
    replace(seq, 'T'=>'U')
end

function readfirstline(filename)
    open(filename, "r") do f
        return f |> readline
    end
end

function checklength(seq)
    @assert length(seq) > 0 "Length of input sequence must be >0"
    seq
end

function main()
    (isempty(ARGS) ? readline() : ARGS[1] |> readfirstline) |>
    checklength |>
    transcribe |>
    println
end


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end

