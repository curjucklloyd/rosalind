#!/usr/bin/env julia


function main()
    n = 0
    open(ARGS[1], "r") do f
        n = parse(Int, readline(f))
    end
    @assert 3<=n<=10000 "3 ≤ n ≤ 10000"
    println(n-2)
end


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end
