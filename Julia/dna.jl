#!/usr/bin/env julia


const ORDERED_LETTERS = ['A','C','G','T']

function counter(seq)
    ORDERED_LETTERS .|> (x->count(x, seq)) |> x->join(x, ' ')
end

function readfirstline(filename)
    open(filename, "r") do f
        return f |> readline
    end
end

function checklength(seq)
    @assert length(seq) > 0 "Length of input sequence must be >0"
    seq
end

function main()
    (isempty(ARGS) ? readline() : ARGS[1] |> readfirstline) |>
    checklength |>
    counter |>
    println
end


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end

