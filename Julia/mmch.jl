#!/usr/bin/env julia


function main()
    seq = ""
    open(ARGS[1], "r") do f
        readline(f) #skip the first one
        seq = join(readlines(f), "")
    end
    
    a,g,c,u = map(x->count(==(x), seq), ['A','G','C','U'])
    factorial(big(maximum([a,u]))) // factorial(big(maximum([a,u])-minimum([a,u]))) *
    factorial(big(maximum([g,c]))) // factorial(big(maximum([g,c])-minimum([g,c]))) |> numerator |> println
end


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end
