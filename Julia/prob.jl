#!/usr/bin/env julia
using Pipe: @pipe


lg(x) = log(10, x)

function prob(x, l, n_AT, n_GC)
    round(
        -l*lg(2) + n_AT*lg(1-x) + n_GC*lg(x),
        digits=3
    )
end

function printlnsep(args...; separator::String = " ")
    println(join(args, separator))
end

function main()
    seq = ""
    gcs = []
    open(ARGS[1], "r") do f
        seq = readline(f)
        gcs = @pipe f |> readline |> split .|> parse(Float32, _)
    end
    
    n_AT, n_GC = @pipe ['A','T','G','C'] .|> count(==(_), seq) |> sum.([_[1:2], _[3:4]])
    ans = prob.(gcs, length(seq), n_AT, n_GC)
    printlnsep(ans...)
end


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end
