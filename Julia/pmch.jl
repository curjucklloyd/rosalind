#!/usr/bin/env julia


function main()
    seq = ""
    open(ARGS[1], "r") do f
        readline(f) #skip the first one
        seq = join(readlines(f), "")
    end
    n_A, n_G, n_C, n_U = map(x-> count(x, seq), ["A", "G", "C", "U"])
    @assert (n_A==n_U) & (n_G==n_C) "Number of base occurences mismatch"
    perfect_matchings = factorial(big(n_A)) * factorial(big(n_C))
    println(perfect_matchings)
end


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end
