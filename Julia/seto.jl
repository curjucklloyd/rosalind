#!/usr/bin/env julia

function toset(s)
    s[2:end-1] |>
    x -> split(x, ", ") |>
    x -> parse.(Int, x) |>
    Set
end

function printlnsep(io=stdout, args...; sep=" ", endline="\n",fst="",lst="")
    print(io, fst*join(args, sep)*lst*endline)
end

function main()
    superset = Set()
    A=Set()
    B=Set()
    open(ARGS[1], "r") do f
        superset = [1:parse(Int, readline(f));] |> Set
        A = f |> readline |> toset
        B = f |> readline |> toset
    end

    printlnsep(A ∪ B...;                fst='{',lst='}',sep=", ")
    printlnsep(A ∩ B...;                fst='{',lst='}',sep=", ")
    printlnsep(setdiff(A, B)...;        fst='{',lst='}',sep=", ")
    printlnsep(setdiff(B, A)...;        fst='{',lst='}',sep=", ")
    printlnsep(setdiff(superset, A)...; fst='{',lst='}',sep=", ")
    printlnsep(setdiff(superset, B)...; fst='{',lst='}',sep=", ")
end


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end
