# Solutions for bioinformatics tasks on [Rosalind](https://rosalind.info)
Initial commit brings all the Python scripts, [written](https://rosalind.info/users/alex_feb2021/) in march 2021 primarily for [bioinformatics university](https://bioinf.me) admissions. Since then I've tried a few other programming languages. Latter scripts, if any, will be uploaded straight away.

## How to use:
All scripts recieve name of testing file (which Rosalind sends you on each attempt) as the only comand line argument. Output is printing to `STDOUT` and can be redirected to file using `>`:

### Running in Windows:
```sh
cd Languagename # Go to subdirectory of chosen language
python scriptname.py inputfile > outfile
julia scriptname.jl inputfile > outfile
```
### In Linux you alternatively can:
```sh
cd Languagename
chmod +x scriptname # make file executable
./scriptname inputfile > outfile # make sure, your terminal knows the interpreter name, stated after the shebang
```
Also, `dna.jl`, `rna.jl`, `revc.jl` support unix-way piping. Try something like:
```sh
echo 'ATCGCGGACTT' | julia revc.jl | julia dna.jl
```



# <img src="https://rosalind.info/static/img/locations/bioinformatics-stronghold.png?v=1637535648" alt="Bioinformatics Stronghold" width="60"/>  Solutions for [Bioinformatics Stronghold](https://rosalind.info/problems/list-view/):


| **Title**                                        | **Python**                | **Julia**                |
|--------------------------------------------------|---------------------------|--------------------------|
| Counting DNA Nucleotides                         | [dna.py](Python/dna.py)   | [dna.jl](Julia/dna.jl)   |
| Transcribing   DNA into RNA                      | [rna.py](Python/rna.py)   | [rna.jl](Julia/rna.jl)   |
| Complementing   a Strand of DNA                  | [revc.py](Python/revc.py) | [revc.jl](Julia/revc.jl) |
| Rabbits   and Recurrence Relations               | [fib.py](Python/fib.py)   |                          |
| Computing   GC Content                           | [gc.py](Python/gc.py)     |                          |
| Counting   Point Mutations                       | [hamm.py](Python/hamm.py) |                          |
| Mendel's   First Law                             | [iprb.py](Python/iprb.py) |                          |
| Translating   RNA into Protein                   | [prot.py](Python/prot.py) |                          |
| Finding   a Motif in DNA                         | [subs.py](Python/subs.py) |                          |
| Consensus   and Profile                          | [cons.py](Python/cons.py) |                          |
| Mortal   Fibonacci Rabbits                       | [fibd.py](Python/fibd.py) |                          |
| Overlap   Graphs                                 | [grph.py](Python/grph.py) |                          |
| Calculating   Expected Offspring                 | [iev.py](Python/iev.py)   |                          |
| Finding   a Protein Motif                        | [mprt.py](Python/mprt.py) |                          |
| Inferring   mRNA from Protein                    | [mrna.py](Python/mrna.py) |                          |
| Open   Reading Frames                            | [orf.py](Python/orf.py)   |                          |
| Enumerating   Gene Orders                        | [perm.py](Python/perm.py) |                          |
| Calculating   Protein Mass                       | [prtm.py](Python/prtm.py) |                          |
| Locating   Restriction Sites                     | [revp.py](Python/revp.py) |                          |
| RNA   Splicing                                   | [splc.py](Python/splc.py) |                          |
| Enumerating   k-mers Lexicographically           | [lexf.py](Python/lexf.py) |                          |
| Perfect   Matchings and RNA Secondary Structures |                           | [pmch.jl](Julia/pmch.jl) |
| Partial   Permutations                           | [pper.py](Python/pper.py) | [pper.jl](Julia/pper.jl) |
| Introduction   to Random Strings                 |                           | [prob.jl](Julia/prob.jl) |
| Enumerating   Oriented Gene Orderings            | [sign.py](Python/sign.py) |                          |
| Transitions   and Transversions                  | [tran.py](Python/tran.py) |                          |
| Completing   a Tree                              |                           | [tree.jl](Julia/tree.jl) |
| Counting   Phylogenetic Ancestors                |                           | [inod.jl](Julia/inod.jl) |
| k-Mer   Composition                              | [kmer.py](Python/kmer.py) | [kmer.jl](Julia/kmer.jl) |
| Maximum   Matchings and RNA Secondary Structures |                           | [mmch.jl](Julia/mmch.jl) |
| Creating   a Distance Matrix                     |                           | [pdst.jl](Julia/pdst.jl) |
| Counting   Subsets                               |                           | [sset.jl](Julia/sset.jl) |
| Introduction to Set Operations                   |                           | [seto.jl](Julia/seto.jl) |
