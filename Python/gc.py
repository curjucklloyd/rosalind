#!/usr/bin/env python3
from sys import argv, stderr
import re
import os


def gc(seq:str) -> float:
    seq = tuple(seq)
    g_c = seq.count('G') + seq.count('C')
    return 100 * g_c/len(seq)

def get_max_gc(content:list) -> tuple:
    name_max = ''
    maxGC = 0.
    for name, seq in zip(content[0::2], content[1::2]): # kinda .fasta reader
        gc_seq = gc(seq)
        if gc_seq > maxGC:
            name_max, maxGC = name[1::], gc_seq
    return name_max, maxGC

def main():        
    with open(argv[1], 'r') as f:
        fasta = re.split('(>Rosalind_[0-9]*)', ''.join(f.read().split()))[1:]
    assert len(fasta) > 1, "Input is too short"
    print(*get_max_gc(fasta), sep='\n')


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)

