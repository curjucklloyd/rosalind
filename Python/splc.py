#!/usr/bin/env python3
from sys import argv, stderr
import re


COMPLEMENTS = {
    'A':'T',
    'T':'A',
    'G':'C',
    'C':'G'
}

DNA_CODONS = {
    'TTT':'F',   'CTT':'L', 'ATT':'I', 'GTT':'V',
    'TTC':'F',   'CTC':'L', 'ATC':'I', 'GTC':'V',
    'TTA':'L',   'CTA':'L', 'ATA':'I', 'GTA':'V',
    'TTG':'L',   'CTG':'L', 'ATG':'M', 'GTG':'V',
    'TCT':'S',   'CCT':'P', 'ACT':'T', 'GCT':'A',
    'TCC':'S',   'CCC':'P', 'ACC':'T', 'GCC':'A',
    'TCA':'S',   'CCA':'P', 'ACA':'T', 'GCA':'A',
    'TCG':'S',   'CCG':'P', 'ACG':'T', 'GCG':'A',
    'TAT':'Y',   'CAT':'H', 'AAT':'N', 'GAT':'D',
    'TAC':'Y',   'CAC':'H', 'AAC':'N', 'GAC':'D',
    'TAA':False, 'CAA':'Q', 'AAA':'K', 'GAA':'E',
    'TAG':False, 'CAG':'Q', 'AAG':'K', 'GAG':'E',
    'TGT':'C',   'CGT':'R', 'AGT':'S', 'GGT':'G',
    'TGC':'C',   'CGC':'R', 'AGC':'S', 'GGC':'G',
    'TGA':False, 'CGA':'R', 'AGA':'R', 'GGA':'G',
    'TGG':'W',   'CGG':'R', 'AGG':'R', 'GGG':'G',
}

def fasta(filename:str) -> list: # of tuples
    """Returns the list: [ (Name, Sequence) ] for a given filename
    """
    with open(filename, 'r') as source:
        lines = re.split('(>Rosalind_[0-9]*)',''.join(source.read().split()))[1:]
        assert lines[0].startswith('>'), "FASTA format required"
        return lines

def reverse_complement(seq:str) -> str:
    return "".join([COMPLEMENTS[base] for base in seq[::-1]])

def ORF(line:list) -> list: #of tuples
    """Returns [(start1,stop1),(start2,stop2),(start3,stop3)] - 
    indexes of RF for specific line length, which are sutable
    for inverse complement as well.
    """
    L = len(line)
    return [(0,L-L%3), (1,L-(L-1)%3), (2,L-(L-2)%3)]

def trans_late(seq:str, start:int, stop:int, out:set, full=None) -> str:
    #reassigning full to None each time to invoke empty list
    if isinstance(full, type(None)):
        full = []
    reading = False #every protein starts with ATG
    done = False    #only proteins, ending with Stop are translated
    
    for n in range(start, stop,3):        
        #current triplet:
        new = DNA_CODONS[seq[n]+seq[n+1]+seq[n+2]]
        if new == 'M':
            #recursive case for proteins that share single stop-codone
            trans_late(seq, n+3, stop, out)        
            reading = True
        if reading:
            if new:
                full.append(new)
            else:
                done = True
                break
        else: continue
    if done:
        out.add(''.join(full))
        
def translate(seq:str, out:set):
    frames = ORF(seq)
    for strand in [seq, reverse_complement(seq)]:
        for fr in frames:
            start, stop = fr[0], fr[1]
            trans_late(seq, start, stop, out)
    
def main():
    """Takes name of file in FASTA format and prints all the possible proteins,
    that first sequence can be translated into, after deleting the introns,
    presented in the next lines.
    """
    master, introns = fasta(argv[1])[1], fasta(argv[1])[3::2]
    for intr in introns:
        master = ''.join(master.split(intr))
    out = set()
    translate(master, out)
    max(out, key=len)
    longest = max(out, key=len)
    print(longest)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
