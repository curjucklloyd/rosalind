#!/usr/bin/env python3
from sys import argv, stderr, exit
import requests, re


def main():
    with open(argv[1], 'r') as proteins:
        labels = proteins.read().split()

    strands = []
    #list for all sequence found

    for protein in labels:
        requestURL = f"http://www.uniprot.org/uniprot/{protein.split('_')[0]}.fasta"

        r = requests.get(requestURL, headers={"Accept": "text/x-fasta"})

        if not r.ok:
        #from uniprot standart parsing form
            r.raise_for_status()
            exit()

        strands.append(''.join(r.text.split('\n')[1:]))
        #getting bare sequences (no labels)

    allmatches = []
    for strand in strands:
        matches = []
        for match in re.finditer("N(?=[^P][ST][^P])", strand): # (?= ) for overlapping
            matches.append(match.start()+1)
        allmatches.append(matches)

    assert len(allmatches) == len(labels)
    #just in case

    for i in range(len(labels)):
        if allmatches[i] != []:
            print(labels[i])
            print(*allmatches[i])
            
            
if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
