#!/usr/bin/env python3
from sys import argv, stderr
from itertools import permutations as perm


def main():
    with open(argv[1], 'r') as f:
        n = int(f.readline())
    p = list(perm(range(1, n+1)))
    print(len(p))
    for i in p:
        print(*i)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
