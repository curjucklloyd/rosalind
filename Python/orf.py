#!/usr/bin/env python3
from sys import argv, stderr
import re


COMPLEMENTS = {
    'A':'T',
    'T':'A',
    'G':'C',
    'C':'G'
}

DNA_CODONS = {
    'TTT':'F',   'CTT':'L', 'ATT':'I', 'GTT':'V',
    'TTC':'F',   'CTC':'L', 'ATC':'I', 'GTC':'V',
    'TTA':'L',   'CTA':'L', 'ATA':'I', 'GTA':'V',
    'TTG':'L',   'CTG':'L', 'ATG':'M', 'GTG':'V',
    'TCT':'S',   'CCT':'P', 'ACT':'T', 'GCT':'A',
    'TCC':'S',   'CCC':'P', 'ACC':'T', 'GCC':'A',
    'TCA':'S',   'CCA':'P', 'ACA':'T', 'GCA':'A',
    'TCG':'S',   'CCG':'P', 'ACG':'T', 'GCG':'A',
    'TAT':'Y',   'CAT':'H', 'AAT':'N', 'GAT':'D',
    'TAC':'Y',   'CAC':'H', 'AAC':'N', 'GAC':'D',
    'TAA':'Stop','CAA':'Q', 'AAA':'K', 'GAA':'E',
    'TAG':'Stop','CAG':'Q', 'AAG':'K', 'GAG':'E',
    'TGT':'C',   'CGT':'R', 'AGT':'S', 'GGT':'G',
    'TGC':'C',   'CGC':'R', 'AGC':'S', 'GGC':'G',
    'TGA':'Stop','CGA':'R', 'AGA':'R', 'GGA':'G',
    'TGG':'W',   'CGG':'R', 'AGG':'R', 'GGG':'G'
}

def d2a(triplet) -> str:
    """DNA triplets to AA conversion"""
    return DNA_CODONS[''.join(triplet)]

def reverse_complement(seq:str) -> str:
    return "".join([COMPLEMENTS[base] for base in seq[::-1]])

def frames(line) -> list: #of tuples
    """Returns [(start1,stop1),(start2,stop2),(start3,stop3)] - 
    indexes of RF for specific line length, which are sutable
    for inverse complement as well.
    """
    L = len(line)
    frames = [(0,L-L%3),
              (1,L-(L-1)%3),
              (2,L-(L-2)%3)]
    return frames

def translate(out, seq, start, stop, full=None) -> str:
    """For recursion purposes. Catches substrings,
    with same Stop- but different Start-codons
    """
    if isinstance(full, type(None)):
        full = []
    reading = False #translation starts at the beginning of the codone
    done = False    #translate only the proteinsthat end with stop-codone
    for n in range(start, stop,3): #
        new = d2a(seq[n]+seq[n+1]+seq[n+2]) # current triplet
        if new == 'M':
            translate(out, seq, n+3, stop) # a bit of recursion :)
            reading = True
        if reading:
            if new != 'Stop':
                full.append(new)
            else:
                done = True
                break
        else: continue
    if done:
        out.add(''.join(full))

def main():
    with open(argv[1], 'r') as source:
        content = re.split('(>Rosalind_[0-9]*)', ''.join(source.read().split('\n')))[1:]
    strand = content[1] #no need in sequence name
    out = set() # to get only unique proteins
    for x in [strand, reverse_complement(strand)]: # forvard and revcompl
        for frame in frames(strand): # for each reading frame
            start, stop = frame[0], frame[1]-2 #starting indices of first and last triplet
            translate(out, x, start, stop)
    print(*out, sep='\n')


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
