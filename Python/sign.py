#!/usr/bin/env python3
from sys import argv, stderr
import itertools as i
import numpy as np


def main():
    with open(argv[1], 'r') as f:
        n = int(f.readline())
    modules = [x for x in range(1,n+1)]
    ints = np.array([x for x in i.permutations(modules, n)])
    plusmin = np.array([x for x in i.product([1,-1], repeat=n)])
    print(len(ints)*len(plusmin))
    for j in ints:
        for k in plusmin:
            print(*j*k)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
