#!/usr/bin/env python3
import re, os
import numpy as np
from sys import argv, stderr


LETTERS = ['A:','C:','G:','T:']
NUM_TO_LETTERS = {0:'A',1:'C',2:'G',3:'T'}

def fasta(filename:str) -> list: # of tuples
    """Returns the list: [ (Head1, Seq1), ..., ] for a given filename
    """
    with open(filename, 'r') as f:
        pre_asrt = f.read()
        assert pre_asrt.startswith('>'), "FASTA format required"
        lines = re.split('(>Rosalind_[0-9]*)',''.join(pre_asrt.split()))[2::2]
        return lines

def num2nucl(arr:np.ndarray) -> str:
    """Converts numbers from ndarray to DNA-sequence
    """
    return ''.join([NUM_TO_LETTERS[num] for num in arr])

def check(data:list) -> None:
    for i in range(1, len(data)):
        assert len(data[i]) == len(data[i-1]), "Can't build proper matrix, sequences have unequal length"
    return len(data[1]), len(data)

def main():
    data = fasta(argv[1])
    seq_len, n_lines = check(data)
    dicts = []
    for i in range(seq_len):
        newdict = {'A':0,'C':0,'G':0,'T':0}
        for n in range(n_lines):
            newdict[data[n][i]] +=1
        dicts.append(newdict)

    cons = np.zeros(seq_len, dtype='int8')
    a = np.array([[*d.values()] for d in dicts]).reshape(seq_len,4)
    for row in range(seq_len):
        cons[row] = np.where(a[row] == np.amax(a[row]))[0][0]
    print(num2nucl(cons))
    for i in range(4):
        print(LETTERS[i], *a.T[i])


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
