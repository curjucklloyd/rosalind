#!/usr/bin/env python3
from sys import argv, stderr


def rabbonachi(n:int, k:int) -> int:
    offspring = 1
    mature = 0
    for i in range(n):
        new = k * mature
        mature += offspring
        offspring = new
    return mature


def main():
    with open(argv[1], 'r') as f:
        n, k = map(lambda x: int(x), f.readline().split())
    print(rabbonachi(n, k))


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)

