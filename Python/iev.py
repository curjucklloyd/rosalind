#!/usr/bin/env python3
from sys import argv, stderr
import numpy as np


def main():
    coeffs = np.asarray([1.,1.,1.,0.75,0.5,0.])
    with open(argv[1], 'r') as source:
        ints = np.asarray(list(map(lambda x: int(x), source.readline().split())))
    print(2*np.sum(ints*coeffs))


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
