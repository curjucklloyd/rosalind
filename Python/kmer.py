#!/usr/bin/env python3
from sys import argv, stderr
import re
from itertools import product as prod


ALPHABET = ['A','C','G','T']

def fasta(filename:str) -> list:
    """Returns tuple: [ Head1, Head2, ... ], [ Seq1, Seq2, ... ]
    for a given filename.
    """
    with open(filename, 'r') as f:
        content = f.read()
        assert content.startswith('>'), "FASTA format required"
        lines = re.split('(>Rosalind_[0-9]+)',''.join(content.split()))[1:]
        heads, tails = lines[0::2], lines[1::2]
        return heads, tails

def main():
    line = fasta(argv[1])[1][0]
    lexems = {}
    for p in prod(ALPHABET, repeat=4):
        lexems[''.join(p)] = 0

    for pos in range(len(line)-3): #attention to index offsets!
        lexems[line[pos:pos+4]] += 1

    print(*lexems.values())


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
