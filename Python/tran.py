#!/usr/bin/env python3
from sys import argv, stderr
import re


TRANSITIONS = {'AG', 'CT', 'GA', 'TC'}

def fasta(filename: str) -> list: # of tuples
    """Returns the list: [ Head1, Seq1, ..., ] for a given filename
    """
    with open(filename, 'r') as source:
        before_assertion = source.read()
        assert before_assertion.startswith('>'), "FASTA format required"
        lines = re.split('(>Rosalind_[0-9]*)',''.join(before_assertion.split()))[1:]
        return lines

def main():
    seq1, seq2 = fasta(argv[1])[1], fasta(argv[1])[3]
    assert len(seq1) == len(seq2), "Strings must have equal length"
    trs, trl = 0, 0
    for i in range(len(seq1)):
        if seq1[i] == seq2[i]:
            continue
        if seq1[i] + seq2[i] in TRANSITIONS:
            trs += 1
        else:
            trl += 1
    print(trs/trl)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
