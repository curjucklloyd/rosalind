#!/usr/bin/env python3
from sys import argv, stderr


def deathbonachi(n,m):
    rab = [0]*(m)
    new = 1
    for i in range(n):
        rab.append(new)
        rab.pop(0)
        new = sum(rab[:-1])
        total = sum(rab)
    return total

def main():
    with open(argv[1], 'r') as f:
        n, m = map(lambda x: int(x), f.readline().split())
    print(deathbonachi(n, m))


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
