#!/usr/bin/env python3
from sys import argv, stderr


def search(substr:str, string:str) -> int:
    i = string.find(substr)
    while i != -1:
        yield i+1
        i = string.find(substr, i+1)
    
def main():
    with open(argv[1], 'r') as f:
        string, substr = f.read().split()
    occurances = [pos for pos in search(substr, string)]
    print(*occurances)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
