#!/usr/bin/env python3
from sys import argv, stderr


N_TRIPLETS = {
    'F':2,'L':6,'I':3,'M':1,
    'V':4,'S':6,'P':4,'T':4,
    'A':4,'Y':2,'H':2,'Q':2,
    'N':2,'K':2,'D':2,'E':2,
    'C':2,'W':1,'R':6,'G':4,
}

def main():
    """Works with linear O(n) time (instead of O(n^2)).
    Much faster with longer (>100kaa) sequences.
    """
    with open(argv[1], 'r') as f:
        seq = f.read().split('\n')[0]
    total = 3
    for aminoac in seq:
        total *= N_TRIPLETS[aminoac]
        total %= 1_000_000
        
    total = int(total)
    print(total)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
