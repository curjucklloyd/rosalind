#!/usr/bin/env python3
from sys import argv, stderr


COMPLEMENTS = {
    'A':'T',
    'T':'A',
    'G':'C',
    'C':'G'
}

def reverse_complement(seq:str) -> str:
    return "".join([COMPLEMENTS[base] for base in seq[::-1]])

def main():
    with open(argv[1], 'r') as f:
        seq = ''.join(f.readline().split())
    assert len(seq) != 0, "Length of input sequence must be >0"
    print(reverse_complement(seq))


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)

