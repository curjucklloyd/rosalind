#!/usr/bin/env python3
from sys import argv, stderr


def pper(n, k):
    return (n if k == 1 else (n-k+1) * pper(n, k-1)) % 1_000_000

def main():
    with open(argv[1], 'r') as f:
        n, k = map(int, f.readline().split())
    print(pper(n, k))


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
