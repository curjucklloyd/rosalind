#!/usr/bin/env python3
from sys import argv, stderr


def prob(k:int, m:int, n:int) -> float:
    t = k+m+n
    return (k*(k-1+m+n) + m*(k+0.75*(m-1) + n/2) + n*(k+m/2)) / (t**2-t)
    
def main():
    with open(argv[1], 'r') as f:
        k, m, n = map(lambda x: int(x), f.readline().split())
    print(prob(k,m,n))


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
