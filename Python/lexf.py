#!/usr/bin/env python3
from sys import argv, stderr
from itertools import product as prod


def main():
    with open(argv[1], 'r') as f:
        alphabet = f.readline().split()
        alphabet.sort()
        n = int(f.readline())
    for p in prod(alphabet, repeat=n):
        print(*p, sep='')


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
