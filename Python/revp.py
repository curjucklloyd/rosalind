#!/usr/bin/env python3
from sys import argv, stderr
import re

SHORTEST_STRING = 4
LONGEST_STRING = 12
COMPLEMENTS = {
    'A':'T',
    'T':'A',
    'G':'C',
    'C':'G'
}

def second_line(filename:str) -> list:
    with open(filename, 'r') as f:
        assert f.readline().startswith('>'), "FASTA format required"
        return ''.join(f.read().split())

def reverse_complement(seq:str) -> str:
    return "".join([COMPLEMENTS[base] for base in seq[::-1]])

def check(seq:list, n:int, heap:set) -> None:
    for pos in range(len(seq)-n+1):
        substr = seq[pos:pos+n]
        if substr == reverse_complement(substr):
            heap.add((pos+1, n))

def main():
    S = second_line(argv[1])
    total = set()
    for n in range(SHORTEST_STRING, LONGEST_STRING+1, 2):
        check(S, n, total)
    print(total)
    for occurance in total:
        print(*occurance)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
