#!/usr/bin/env python3
from sys import argv, stderr
import re


def fasta(filename:str) -> list:
    """Returns tuple: [ Head1, Head2, ... ], [ Seq1, Seq2, ... ]
    for a given filename.
    """
    with open(filename, 'r') as source:
        before_assertion = source.read()
        assert before_assertion.startswith('>'), "FASTA format required"
            
        lines = re.split('(>Rosalind_[0-9]+)',''.join(before_assertion.split()))[1:]
        heads, tails = lines[::2], lines[1::2]
        return heads, tails

def check_next(names: list,sequences: list,n: int,out='out.txt') -> None:
    """Writes in file adjacency list corresponding to O_n
    """
    for i in range(len(sequences)):      # for each edge
        for j in range(len(sequences)):  # checking each edge
            if i == j:                   # excluding itself
                continue
            elif sequences[i][-n:] == sequences[j][:n]:
                print(names[i][1:], names[j][1:])

def main():
    O_n = 3
    heads, tails = fasta(argv[1])
    check_next(heads,tails,O_n)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
