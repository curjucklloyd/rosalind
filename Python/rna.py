#!/usr/bin/env python3
from sys import argv, stderr


def transcribe(seq:str) -> str:
    return seq.replace('T','U')

def main():
    with open(argv[1], 'r') as f:
        seq = ''.join(f.readline().split())
    assert len(seq) != 0, "Length of input sequence must be >0"
    print(transcribe(seq))


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)

