#!/usr/bin/env python3
from sys import argv, stderr


def hamming(s1:str, s2:str) -> int:
        metric = 0
        for l1, l2 in zip(s1,s2):
            if l1 != l2: metric += 1
        return metric

def main():
    with open(argv[1], 'r') as source:
        s1, s2 = source.read().split()
    assert len(s1) == len(s2), "Input strings must have equal length"
    print(hamming(s1, s2))


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e, file=stderr)
